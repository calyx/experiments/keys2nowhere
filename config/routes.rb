# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

Rails.application.routes.draw do
  mount AuthenticatedApp::Engine => "/auth"

  get '/join' => "enrollments#join"
  post '/join' => "enrollments#create"

  get 'messages' => "messages#index"

  root "root#index"
end
