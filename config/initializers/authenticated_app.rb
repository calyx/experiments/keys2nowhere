AuthenticatedApp.configure do |config|
  config.home_url = :root_path  # home for authenticated users
  config.root_url = :root_path  # home for unauthenticated users

  # if true, the 'login' name is the same as email.
  config.use_email_for_login = true

  # array of strings to disallow as usernames
  config.forbidden_usernames = ['root*', 'admin*']

  # allow recovery using user#email
  config.allow_recovery_from_primary_email = false
end
