class User < ApplicationRecord
  include AuthenticatedApp::UserAuthentication
  include AuthenticatedApp::UserRecoveryEmail

  validates :username, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: { case_sensitive: false }

  before_validation { self.email&.downcase! }

  def login
    username
  end

  def is_active?
    true
  end

  def is_suspended?
    false
  end

  def is_pending?
    false
  end

  def self.find_by_login(login)
    find_by(username: login) || find_by(email: login)
  end

  def self.enroll_user(params)
    transaction do
      create!(username: params.require(:username),
              email: params.require(:email))
        .update_password!(password: params.require(:password),
                          password_confirmation: params.require(:password_confirmation))
    end
  end
end
