module ApplicationHelper
  def user_signed_in?
    controller.current_user.present?
  end
end
