class EnrollmentsController < ApplicationController
  skip_before_action :require_authentication

  def join
  end

  def create
    User.enroll_user(params)
    redirect_to(root_path, notice: "Thank you. Please sign in.")
  rescue ActiveRecord::RecordInvalid => invalid_err
    @errors = invalid_err.record.errors
    render :join
  rescue AuthenticatedApp::PasswordUpdateError => password_err
    @errors = password_err.errors
    render :join
  end
end
