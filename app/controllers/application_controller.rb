class ApplicationController < ActionController::Base
  include AuthenticatedApp::ControllerConcern

  def is_authorized?(user)
    true
  end
end
