class RootController < ApplicationController
  skip_before_action :require_authentication
  before_action :optional_authentication

  def index
  end
end
