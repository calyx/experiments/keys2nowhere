require "test_helper"

class EnrollmentsControllerTest < ActionDispatch::IntegrationTest
  test "should get join" do
    get join_url
    assert_response :success
  end

  test "should enroll user" do
    post join_url params: {
                    username: "foobar123",
                    email: "foobar123@example.com",
                    password: "keypejciditshIrWytwo",
                    password_confirmation: "keypejciditshIrWytwo"
                  }

    assert_redirected_to root_path
    follow_redirect!
    assert_select "div.alert", /Thank you/
  end
end
