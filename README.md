# Keys 2 Nowhere

Example application utilizing AuthenticatedApp for user Authentication.

``` sh
# install gems and prepare db
bin/setup
# run rails server
bin/dev
# compile
bin/rails assets:precompile
# compile just css
bin/rails dartsass:build
# add js packages
./bin/importmap pin bootstrap@4 --download
```
